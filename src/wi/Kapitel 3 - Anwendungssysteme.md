# Kapitel 3: Anwendungssysteme

---

**Q**: Nennen Sie betriebliche Arbeitsgebiete von Anwendungssystemen

**A**: Das sind:

* **Operative Systeme** (Administrations- und Dispositionssysteme)  

    - (A) Abrechnung von Massendaten
    - (A) Verwaltung von z.B. Konten
    - (D) Vertrieb
    - (D) Produktion
    - (D) Personalwirtschaft

    * Branchenneutrale
        * Für Bereiche, die keine großen branchenspezifische Differenzierung erfordern
            * Buchhaltung
            * Lohn- Gehaltsabrechnung
            * Fakturierung
    * Branchenspezifische
        * Fertigungsindustrie
        * Handelsunternehmen
        * Banken
        * Versicherungen
* Elektronischer Datenaustausch (**EDI**)
    * Austausch von strukturierten Daten zwischen den Unternehmen    
* **Querschnittsysteme**
    * Keine eindeutige Zuordnung möglich
        * Bürosysteme
        * Office-Anwendungen
        * Wissensbasierte Systeme
* **Führungssysteme**
    * Führungsinformationssysteme, z.B. Data Mining
        * Auf den Bedarf des höheren Managements ausgerichtet
        * Wesentliche Punkte:
            * Auswahl der relevanten Information
            * Beschaffung und Aufbereitung der Information
            * Darstellung der Information
    * Planungssysteme, z.B. Tabellenkalkulation
        * Planungunterstützung für Management in Sinne des Managementzyklus
            * Langfristige und strategische Entscheidungen
            * Festlegung von Maßnahmen für das gesamte Unternehmen

---

**Q**: Was sind **Vorteile** eines integrierten AwS?

**A**: Das sind:

* Keine mehrfache Datenerfassung
* Einheitliche Datenstruktur
* Vermeidung von Redundanzen und Inkonsistenzen
* Optimierung und Automatisierung von Abläufen
* Einheitliche Gestaltung von Oberflächen

---

**Q**: Was sind **Nachteile** eines integrierten AwS?

**A**: Das sind:

* Komplexeres Gesamtsystem
* Größere Auswirkung bei Fehlern

---

**Q**: Was sind die Integrationsziele nach Ferstl?

**A**: Das sind:

* **Optimaler Grad** der Redundanz
    * Daten-Redundanz
    * Funktion-Redundanz
* **Auswirkung** der Redundanz
    * Geringe Redundanz
        * Wenige Inkonsistenzen
        *  Wirtschaftlicher Umgang mit Ressourcen
    * Hohe Redundanz
        * Toleranz gegenüber Komponentenausfall
        * Leistungssteigerung bei paralleler Nutzung
* **Kontrolle** der Kommunikationskanäler zwischen Teilen des AwS
    *  Semantische und operationale Integrität

---

**Q**: Beschreiben Sie Facetten der Integration von AwS

**A**: Das sind:

1. **Integrationsgegenstand**
    1. Daten (Datenbestände werden logisch zusammengeführt)
        * Automatische Datenweitergabe
            * Vermeidet mehrfache Datenerfassung
            * Erfordert ein einheitliches Datenformat
        * Gemeinsame Datenbanken
            * Verhindert zusätzlich Redundanzen
            * Ermöglicht Konsistenzüberwachung
    2. Funktion (Aufgaben werden aufeinander abgestimmt)      
    3. Prozesse
    4. Methoden
    5. Programme
        * Integration der Schnittstellen
        * Medien
        * Geräte
2. **Integrationsrichtung**
    1. Horizontal (Integration  innerhalb des Prozesses der Leistungserstellung)
    2. Vertikal (Integration der Operativen Systemen mit PuK)
3. **Integrationsreichweite**
    1. Bereichsintegration
    2. Funktionsbereich- und Prozessübegreifende Integration
    3. Innerbetriebliche Integration
    4. Zwischenbetriebliche Integration
4. **Automatisierungsgrad**
    1. Vollautomation
        * System löst bearbeitung aus
    2. Teilautomation
        * Initiative vom Menschen
        * Initiative vom Computer
        
---

**Q**: Welche Eigenschaften besitzen Datenbanken?

**A**: Sie besitzen die folgenden Eigenschaften:

* Dauerhafte Speicherung und Ausfallsicherheit
* Verwaltung großer Datenmengen
* Redundanz-frei und Konsistenz-sichernd
* Flexibler Zugriff durch mächtige Sprachen
* Effizienter Zugriff auf Daten
* Mehrbenutzerbetrieb
* Zugriffskontrolle
* Kopplung mit Anwendungssystemen (API)

---

**Q**: Was sind zentrale Ideen von Datenbanken?

**A**: Das sind:

* Aufhebung der engen Verflechtung von Daten und Operationen
* Organisation der Daten als Betriebsmittel
* Zugriff auf Daten über Kontrollinstanzen

---

**Q**: Was ist ein **E**nterprise **R**esource **P**lanning-System?

**A**: Das ist ein integriertes Gesamtsystem für alle wesentlichen Funktionen der Administration Disposition und  Führung

Besteht aus:

* Kontrolle
* Beschaffung
* Vertrieb
* Projektmanagement
* Erweiterung über Unternehmensgrenzen hinaus

---

**Q**: Was ist eine Middleware?

**A**: Das ist eine systemnahe Softwareschicht zwischen Systemsoftware und Anwendungssoftware

Unterstützt die Realisierung verteilter Anwendungssysteme durch:

* Bereitstellung höherer Dienste
* Standardisierung der Kommunikation zwischen einzelnen Komponenten
* Genormte API's
* Integration  von Software unterschiedlicher Anbieter

Technologien:

* CORBA
* Enterprise Java Beans
* COM
* Dot.Net
* Web Services

---

**Q**: Was ist eine Componentenware?

**A**: Wiederverwendbares, abgeschlossenes und vermarktbares Softwaremodul, das über eine wohldefinierte API verfügt

---

**Q**: Was ist ein Geschäftsprozess?

**A**: Eine zusammenhängene Abfolge von Unternehmungsverrichtungen zum Zweck einer Leistungserstellung

---

**Q**: Was ist Grundlegendes am ARIS? Welche GP-Typen kennen Sie?

**A**: Das sind:

1. Am GP beteiligte **Aufgabenträgen** & **Beziehungen**
2. **Funktionsfluss**, z.B. dynamisches Verhalten
3. **Leistungsfluss**, z.B. Arbeitsergebnisse
4. **Informationsfluss**, z.B.  ausgetauschte Dokumente

---

**Q**: Zählen Sie die ARIS Sichten auf

**A**: Das sind:

* Organisationssicht
* Datensicht
* Steuerungssicht
    * Modellierung die Beziehungen zwischen den Sichten und damit des GP (**dynamisch**)
* Funkionensicht
* Leistungssicht

---

**Q**: Was ist ARIS **H**ouse **o**f **B**usin**E**ss?

**A**: Organisatorische und informationstechnische Einbettung von ARIS in das Management von GP

Die 4 Ebenen des ARIS HOBE:

1. Prozessgestaltung
2. Prozessplanung
3. Workflowsteuerung
4. Anwendungssystem

---

**Q**: Erläutern Sie die Ihnen bekannten Modellierungsmethoden

**A**: Das sind:

* **Fachkonzept**
    * Funktionssicht, z.B. Ablaufdiagramme
    * Organisationssicht, z.B. Organigramme
    * Datensicht, z.B. ER-Modell
    * Steuerungsicht, z.B. Ereignis-gesteuerte Prozessketten
* **DV-Konzept**
    * Funktionssicht, z.B. Modulentwurf, Schnittstellen
    * Organisationssicht, z.B. Rechensystem, Netztopologien
    * Datensicht, z.B. Datenmodelle
    * Steuerungsicht, z.B. Zuordnungsfunktionen
* **Implementierung**
    * Funktionssicht, z.B. Programmierung
    * Organisationssicht, z.B. Netzwerkkomponenten
    * Datensicht, z.B. SQL
    * Steuerungsicht, z.B. Programmierung, Systemintegration

---

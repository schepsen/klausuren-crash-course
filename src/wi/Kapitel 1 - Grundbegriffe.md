# Kapitel 1:  Grundbegriffe und Selbstverständnis

---

**Q**: Was ist der Erkenntnisgegenstand der Wirtschaftsinformatik?

**A**: Das sind Informationssysteme in Wirtschaft und Gesellschaft, sowohl von Organisationen als auch von Individuen

---

**Q**: Was ist ein Informationssystem?

**A**: Ein Informationssystem ist ein soziotechnisches System, das Informationen verarbeitet

Das sind:

* Menschen
    * Personelle Aufgabenträger
* Organisationen
    * Funktionen, GP, Strukturen und Management
* Informations- und Kommunikationstechnik
    * Maschinelle Aufgabenträger

---

**Q**: Was sind die Aufgaben der WI?

**A**: Das sind:

* **Beschreibung** wahrnehmbarer Sachverhalte mit einer geeigneter Fachsprache
    * Begriffsbildung
    * Erfassung des Forschungsgegenstandes
* **Erklärung**
    * Dokumentation von Ursache- Wirkungsbeziehung
    * Verallgemeinerung von Sachverhälten
* **Prognose**
    * Vorhersage zukünftiger Abläufe und Zustände
* **Gestaltung**
    * Entwicklung von Artefakten unter der berücksichtugung von Zweck-Mittelbeziehung

---

**Q**: Was sind Ziele der WI?

**A**: Das sind:

* **Entwicklung** von Theorien, Methoden und Werkzeugen zur Gewinnung überprüfbarer Erkenntnisse über IS
* Gestaltungsorientierte **Konstruktion** von IS
* Erzielung eines realwissenschaftlichen **Verständnisses** des Einsatzes von IS
* **Bewertung** von Risiko- und Wirtschaftlichkeitsdimensionen bei Gestaltung und Einsatz von IS
* **Prognose** (nicht-)technischer Entwicklungen und Auswirkungen des Einsatzes von IS

---

**Q**: Beschreiben Sie das 5-Phasenmodell in LeShop

**A**: Das sind:

1. Anregungsphase
    * Kunden müssen dazu bewegt werden, aus einem weiteren Teilsortiment zu kaufen
2. Informationsphase
    * Darstellung der Waren in Bilder, Kundenbewertung und Produktsuche
3. Vereinbarungsphase
    * Einfachheit und Transparenz des Kaufprozesses
4. Erfüllungsphase
    * Zusammenstellen der Kundenbestellungen
5. Treuephase
    * Kundendaten gezielt auswerten

---

**Q**: Welche Komponenten des IS gibt es?

**A**: Das sind:

* Informationsverarbeitungsaufgaben (Beziehung: Informationsbeziehung)
    * Lenkungsaufgaben
        * Planung
        * Steuerung
        * Kontrolle
    * Durchführungsaufgaben
* Aufgabenträger (Beziehung: Kommunikationssystem)
    * Maschinelle
        * Rechner
        * Rechnersysteme
    * Persönliche
        * Sachbearbeiter

Informationsverarbeitungsaufgaben werden Aufgabenträgern zugeordnet

---

**Q**: Was versteht man unter dem Begriff "Anwendungssystem"?

**A**: Ein Anwendungssystem ist:

* **Gesamtheit aller Programme** und die zugehörigen Daten für ein konkretes betriebliches Anwendungsgebiet
* Zusätzlich auch die für die Nutzung der AwSW benötigte **Hardware** und die Kommunikationseinrichtungen

---

**Q**: Gebe Beispiele für AwS

**A**: Das sind:

* AwS als Lenkungssystem
    * Elektronischer Terminkalender
    * Projektplanungssystem
* AwS als Leistungssystem
    * Bildbearbeitungssystem für Fotos
    * MS Word bei einem Ghost-Writer

---

**Q**: Was ist eine Anwendungsarchitektur?

**A**: Darunter ist die methodisch fundierte Struktur eines SW-Systems als Teil eins I-und Kommunikationssystem zu verstehen

---

**Q**: Nennen Sie Funktionen der Architektur

**A**: Das sind:

* Abstraktion
    * Abstrakte komprimierte Darstellung der wesentlichen Aspekte eines System
* Strukturierung
    * Beschreibung von Strukturen
* Verständigung
    * Gemeinsame Sprache zwischen allen Beteiligten
* Standardisierung
    * Branchenspezifische Wiederverwendung von Komponenten
* Entwicklung
    * Generalbebauungsplan für die Erstellung eines AwS

---

**Q**: Zählen Sie Phasen in der Anwendungsentwicklung auf

**A**: Das sind:

1. Betriebswirtschaftliche Problemstellung
    * Anforderungen aus betriebswirtschaftlicher Sicht
2. Fachkonzept
    * Beschreibung der Lösung rein fachlich
3. DV-Konzept
    * Beschreibung des AwS im Hinblick auf die einzusetzende Technologie
4. Implementierungstechnik
    * Beschreibung der Anwendungssoftware, Systemsoftware und Hardware
---

**Q**: Nach welchen Sichten erfolgt die Strukturierung?

**A**: Das sind:

* Datensicht
    * Beschreibung der Daten aller Informationsobjekte
* Funktionssicht
    * Beschreibung der Elemente, die Informationen austauschen und transformieren
* Steuerungssicht
    * Beschreibung aller Abläufe und Interaktionen
* Organisationssicht
    * Organisationseinheiten
    * Ressourcen
    * Organigramme

---

**Q**: Was ist ein Referenzmodell?

**A**: Referenzmodelle spiegeln nicht die Gegebenheiten eines spezifischen Objekts wider und besitzen einen höheren Abstraktionsgrad

P.S. Es ist lediglich nur eine **Empfehlung**!

---

**Q**: Was sind Gütekriterien für Referenzmodelle?

**A**: Das sind:

* Verständlichkeit
    * Verständlich für den Personenkreis, in dem es eingesetzt wird
* Allgemeingültigkeit
    * Für eine möglich große Klasse von Anwendungsproblemen einsetzbar
* Flexobilität
    * Gut anpassbar auf spezielle Situationen
* Nützlichkeit
    * Die daraus entwickelten Systeme sollen wünschenswert gut sein 

---

# Kapitel 4: SOA und Webtechnologien

---

**Q**: Was ist SOA?

**A**: **S**ervice-**O**rientierte **A**rchitekturen bestehen aus voneinander unabhängigen Diensten

---

**Q**: Nennen Sie das Paradigma der SOA

**A**: Das sind:

* Lose gekoppelte SW-Komponenten
* Komponenten bieten Dienste an
* Komponenten kommunizieren über ein Netzwerk miteinander

---

**Q**: Nennen Sie die Rollen der SOA

**A**: Das sind:

* Service-**Requestor**
    * möchte einen Dienst nutzen
* Service-**Provider**
    * stellt Dienste zur Verfügung
* Service-**Broker**
    * findet passende Provider für einen Requestor

---

**Q**: Listen Sie **Anforderungen** der SOA-Architektur auf

**A**: Das sind:

* Robustheit, Performance, Sicherheit und Skalierbarkeit
* Einfache Interkomponentare Kommunikation
* Einfache  und flexible Änderung, Verbesserung und Erweiterung

---

**Q**: Nennen Sie wichtige **Merkmale** von Komponenten

**A**: Das sind:

* Entwicklung individueller Anwendungen durch **Zusammensetzten** mehrerer Komponenten
* **Middleware** als Basisinfrastruktur für Komponenten
* Wiederverwendbares und **vermarktbares** Softwaremodul, das über eine wohldefinierte API verfügt

---

**Q**: Was sind **Vorteile** von Komponenten?

**A**: Das sind:

* Kapselung von Geschäftlogiken in eigenständige Module
* Überschaubare Größenordung
* Zuständigkeitstrennung
* Einfache Wiederverwendbarkeit
* Einfache Austauschbarkeit
* Reduzierung der Total Cost of Ownership (**TCO**)

---

**Q**: Listen Sie Schichten von Komponenten auf

**A**: Das sind:

* **Client**-Schicht
    * HTML
    * Java Applets
* **Server**-Schicht
    * Servlets
    * CGI
* **Business**-Schicht
    * EJB
    * Web-Services
* Integration- und **Daten**schicht
    * Datenzugriffskomponenten

---

**Q**: Welche Komponenten-Standards und verteilte Anwendungen gibt es denn?

**A**: Es gibt:

* **RPC** (Remote Procedure Call) Ausführung einer Prozedur auf einem  entfernten Rechner
    * Feststellung des Ortes der entfernt aufgerufenen Prozedur
    * Übertragung der Funktionsparameter
    * Rückübertragung des Berechnungsergebnisses
* **RMI** (Remote Method Invocation) Kommunikation zwischen JAVA Objekten in einer verteilten Umgebung
    * Eingeschränkt auf die Sprache **JAVA**
    * **Registry** Service kennt alle Objekte
    * Ähnlich zu RPC, allerding anstelle einer Prozedur wird eine Methode aufgerufen

---

**Q**: Wie sieht die RPC-Architektur aus?

**A**: Sie besteht aus:

* **Client**
    * Ruft die entfernte Prozedur auf
* **Stub**
    * Implementiert die Schnittstelle zum Aufrufen der Prozedur mit
* **RPC-Layer**
    * Serialisierung der Argumente
* **Transport Layer**
    * TCP/IP-basierte Schicht
* **Skeleton**
    * Nimmt Aufruf entgegen
* **Server**
    * Bietet die entfernte Prozedur an

---

**Q**: Erkläutern Sie die **CORBA**-Architektur

**A**: CORBA ist eine Architektur für verteilte Anwendungen auf der Grundlage offener Standards

* Programmiersprachenunanhängig
* Hardware- und Plattformunanhängig
* Betrioebssystemunabhängig
* Verteilungsunanhängig, kann als Server sowohl als Client fungieren

* **ORB** (Object Request Broker) übernimmt die Kommunikation zwischen Client- und Serverobjekten

---

**Q**: Was ist die Ortstransparenz bei CORBA?

**A**: Client muss den physischen Ort des Serverobjekts nicht kennen

Das Objekt kann sich befinden:

* Im lokalen Prozess
* Im anderen Prozess auf derselben Maschine
* Irgendwo auf einer entfernten Maschine

**IIOP** (Internet Inter-ORB Protokoll) sorgt für die Kommunikation zwischen ORBs

---

**Q**: Was ist **I**nterface **D**efinition **L**anguage?

**A**: Programmiersprachenunabhängiges Mittel zur Beschreibung von Datentypen und Objektinterfaces

* Methode zur Integration und zum verwenden von Objekten
* Rein **deskriptives** Mittel
* Abstrahiert von der Implementierung
* Erlaubt Anbindung an unterschiedliche Sprachen

---

**Q**: Was sind CORBA Objekte?

**A**: Virtuelle Einheiten, die eine Identität haben, CORBA Objekten sind **keine Komponenten**

---

**Q**: Was bietet **E**nterprise **J**ava **B**eans den Entwickler an?

**A**: EJB bietet:

* Automatische Transaktionsbehandlung
* Portierbarkeit
* Skalierbarkeit
* Automatisches Erzeugen, Löschen und Verteilen von Objekten

---

**Q**: Wie ist **E**nterprise **J**ava **B**eans aufgebaut?

**A**: Die Architektur von EJB ist:

* **Remote** Interface
    * Schnittstelle zu den Clients mit den Kernfunktionen der Komponenten
    * Implementierung ist vollständig verborgen
* **Home** Interface
    * Schnittstelle mit Operationen zur Objektverwaltung
* **Bean** Klasse
    * Eigentliche Implementierung der Operationen
* Deplayoment **Descriptor**
    * XML Datei, die die Komponente (Schnittstellen, Attribute und Operationen) beschreibt

---

**Q**: Welche Arten von **E**nterprise **J**ava **B**eans gibt es denn?

**A**: Man unterscheidet zwischen:

* EJB **Entity**
    * Werden als Datenobjekt verwendet
    * Sind persisent
    * Garantieren die Integrität der Datenbank
* EJB **Session**
    * Implementieren Geschäftslögik (keine GUI)
    * Bilden die Schnittstelle, mit der die Clients mit dem System interagieren
    * Greifen auf mehrere Entity Beans zu
    * Stateful vs. Stateless
---

**Q**: Erläutern Sie die wesentlichen Merkmale von **COM+**

**A**: Das sind:

* **C**omponent **O**bject **M**odul ist Bestandteil von Windows und im wesentlichen darauf beschränkt
* Definiert binären **sprachunabhängigen** Standard zur Objektkommunikation
* Schränkt die Kommunikation auf lokale Objekte ein
* Erstellung von Komponenten auf der Basis von dynamischen Laufzeitbibliotheken

---

**Q**: Erklären Sie die wesentlichen Merkmale von **.Net**

**A**: Das sind:

* Standsrd für verteilte Komponentenapplikationen
* Ablösung für den COM-Standard
* Basiert auf offenen Standards
* Deviceunabhängigkeit
* Softwareentwicklungsumgebung Visual Studio .Net

Programme werden in **MSIL** übersetzt

Wesentliche Bestandteile der .Net Plattform: Smart Clients, Web Services, Development Tools & Plattform

---

**Q**: Was ist ein Web-Service?

**A**: Dienst, der seine Schnittstelle über Standard-Web-Protokolle anbietet

* Der Service wird  durch **WSDL** beschrieben
* Plattform- und Programmiersprachenunabhängig
* Anwendungsfeld ist **B2B**
* Kommunikation mit dem Web-Service erfolgt über Webprotokolle
    * HTTP
    * SMTP
    * SOAP (Simple Object Access Protokoll)
* **UDDI** ist ein Dienstverzeichnis

---

**Q**: Erläutern Sie die SOAP Messaging

**A**: Mögliche Arten:

* One-Way
* Request-Response
* Call-Back Kommunikation
* Notification
* notification-Response

Messages werden durch generische Methoden realisiert, RPC sind allerdings aus Signatur oder Assembly Manifests

---

**Q**: Wie ist WSDL aufgebaut?

**A**: WSDL enthält folgende Attribute:

* **Types**
    * Beschreibung verwendeter Datentypen in XML-Schema
* **Messages**
    * Definition einzelner SOAP-Nachrichten
* **Port** Types
    * Definition einzelner Methode
* **Binding**
    * Spezifikation eines Protokolls
* **Service**
    * Definiert Port/Bindungs-Paare

---

**Q**: Muss man noch was zusätzlich drüber wissen?

**A**: Ja, und zwar:

Web **Clients**:

* Browers bzw. Clients
* HTML und JavaScript
    * Vorteile: Kurze Downloadzeiten
    * Nachteile: Nur einfache Client-Funktionen, Kommunikation bei jeder Benutzerinteraktion

Web **Servers**:

* Bereitstellen von HTML Seiten
* Serverseitige Skripte ausfüheren
* Logging Aufgaben
* The **Apache** HTTP- bzw. nginx-Server

**Business-Logic**:

**CGI** (Common Gateway Interface)

* HTML kann ein CGI Programm aufrufen
* **Vorteile**
    * Einfaches Mittel um dynamische Webinhalte zu erzeugen
    * CGI ist sprachunanhängig
* **Nachteile**:
    * Overhead bei Prozesserzeugung
    * Nur begrenzte Anzahl von Requests kann parallel abgearbeitet werden
* Verbesserung: **Fast CGI**, starte Prozesse auf Vorrat

**Servlets** (Wer Server API für JAVA)

* **Vorteile**:
    * Reine JAVA-basierte Anwendungen können realisiert werden
    * Skaliert durch steigende Anzahl von Instanzen der Servlet Klasse
    * Kommunikation mit Client via RMI möglich

 AJAX (Asynchronous JavaScript and XML)

* Basiert auf dem Einsatz von JavaScript auf Client-Seite
* **Vorteile**:
    * Belibieg aufwendige Clientfunktionen können realisiert werden
* **Nachteile**:
    * JavaScript muss im Browser eingeschaltet werden
    * Viele Technologien, relativ kompliziert in Entwicklung und Wartung

---

**Q**: Erläutern Sie die wichtigsten **Geschäftsmodelle**

**A**: Es gibt:

* GM 1: SW+ & +
    * Web Services sind Bestandteil des Produktfolios
* GM 2: SW+ & -
    * Kostenfrei Web Services für Endkunden, z.B. Als Ergänzung zu  SW mit klassischem  Lizenzmodell
* GM 3: SW- & -
    *  Öffentliche Web Services oder für Kooperationspartner
* GM 4: SW- & +
    * Web Services als neue Erlösquellen

---

**Q**: Welche **Verteilungsformen** kennen Sie?

**A**: Es gibt:

* **Two Tier**
    * (1) Client und  (2) Web Server & Business Logik & DBMS
    * (N) Zu hohe Serverlast, geringe Performance
    * (N) Keine Skalierungsmöglichkeiten
    * (P) Einfach, Kostengünstig
    * (P) Download einer Client-SW ist nicht nötig
* **Three Tiers**
    * (1) Client, (2) Web Server & Business Logik & Application Server und (3) Datenbank Server
    * Serverlast ist zwischen Tier 2 und Tier 3 verteilt
    * Kann auf Tier 2 skaliert werden
* ** Four Tier**
    * (1) Client, (2) Web Server, (3) Application Server & BL und (4) Datenbase Server
    * Hohere Performance als Three Tier
    * Jeder Server kann für die spezielle Aufgabe optimiert werden

---

# Kapitel 5: Forschungstrends der Wirtschaftsinformatik

---

**Q**: Welche Forschungstrends in der Wirtschaftsinformatik gibt es heutzutage?

**A**: Heutige Forschungstrends:

* Cloud Computing
* Wissensmanagement
* Business Intelligence

---

**Q**: Was ist ein Cloud-Dienst?

**A**: Ein im Web verfügbarer Dienst, der keine Installation erfordert, sondern **an-hoc** genutzt werden kann

---

**Q**: Was ist Cloud-Computing?

**A**: CC erlaubt die Bereitstellung und Nutzung von IT-Infrastruktur, von Plattformen und von Anwendungen aller Art als Cloud-Dienste

---

**Q**: Was sind die Merkmale des Cloud-Computing?

**A**: Das sind:

1. On-Demand Self-Service
    * Diensterbringung auf Anfrage, und zwar ohne jegliche menschliche Interaktion
2. Netzwerkbasierter Zugang
    * z.B.  Internet
3. Resource Pooling
    * Ressourcen sind in Pools verfügbar
    * Anpassung erfolgt an tatsächlichen Bedarf
4. Elastizität
    * Illusion unendlich verfügbarer Ressourcen
    * Einfache Skalierbarkeit
5. Messbarkeit: quantative bzw. qualitative Messbarkeit der Dienste

---

**Q**: Was ist eine Virtualisierung?

**A**: Abstrakte, logische Sicht auf physische Ressourcen. P.S. Sie ist die wichtige Voraussetzung für die **ELASTIZITÄT**

---

**Q**: Welche Virtualisierungskonzepte kennen Sie bereits?

**A**: Das sind:

1. Betriebssystemvirtualisierung
    * Betriebssystemkern, auf dem mehrere virtuelle Systemumgebungen laufen      
2. Plattformvirtualisierung
    * Ausführung beliebiger Betriebssysteme und Anwendungen in virtuellen Umgebungen
    * **Hypervisor** ist ein auf ein Minimum reduziertes Metabetriebssystem, das die HW-Ressourcen unter den Gastsystemen verteilt
3. Speichervirtualisierung
    * Datenspeicher ist getrennt von den klassischen File-Servern, z.B. Speichernetzwerk (SAN)
4. Netzwerkvirtualisierung
5. Anwendungsvirtualisierung
    * Dynamische Bereitstellung von SW-Komponenten zum Download oder zum Betrieb über das Internet (hosted application)

---

**Q**: Welche Betriebsmodelle des Cloud-Computing kennen Sie?

**A**: Das sind:

1. Public Cloud
    * Anbieter und potentielle Nutzer gehören nicht derselben organisatorischen Einheit an
2. Private Cloud
    * Anbieter und Nutzer gehören derselben organisatorischen Einheit an
3. Hybrid Cloud
    * Eine Kombination aus Private & Public Clouds, wobei einige Dienste in die Public Clouds ausgelagert werden
4. Community Cloud
    * Die Cloud Infrastruktur gehört mehreren Organisationen an

---

**Q**: Nennen die den möglichen Gegenstand sowie Phasen von Dienstgütevereinbarungen (SLA)

**A**: Das sind:

* Sicherheit der Daten
* Prioritäten
* Verantwortlichkeiten
* Garantien
* Messbare Größe wie Verfügbarkeit, Durchsatz und Reaktionszeiten

* Vereinbarung der Leistungen (QoS)
* Überwachung der Leistungen (Service Monitoring)

---

**Q**: Welche Risiken können Sie nennen?

**A**: Es gibt:

* Verfügbarkeit der Daten
* Lock-in der Daten
* Vertraulichkeit der Daten
* Performance
* Skalierung
* Fehlerbehandlung

---

**Q**: Welche Vorteile kann man durch Einsatz von Cloud Technologien erlangen?

**A**: Das sind:

* Einsparpotential für IT-Kosten
* Wettbewerbsvorteile
* Skaleneffekte durch viele Benutzer

---

**Q**: Welche Probleme begegnet man bei Wissensmanagement?

**A**: Das sind:

* Training ist teuer, und kann Profit einschränken
* Verlässt eine Person ein Unternehmen, so nimmt sie ihr Wissen mit
* Teilung des Wissens bedeutet, Macht abzugeben

Trends bei Wissensmanagement:

* Globalisierung des Wettbewerbs
* Weltweite Partnernetzwerke
* beschleunigte Produktentwicklungszyklen

---

**Q**: Was ist Wissen aus der Informatiksicht?

**A**: Das ist:

* **Deklarativ**
    * Explizites Wissen, das in Datenstrukturen gespeichert ist
* **Prozedural**
    * Implizites Wissen, das man durch automatische Auswertungsprozeduren bekommt

**Implizites** Wissen ist persönlich, kontextspezifisch und daher nur schwer kommunizierbar

**Explizites** Wissen lässt sich in formaler, systematischer Sprache weitergeben

---

**Q**: Was ist Wissensmanagement?

**A**: Ein systematischer Ansatz beid em implizites und explizites Wissen eine strategische Schlüsselrolle spielt, und der zum Ziel hat, den Umfang mit Wissen auf individualler Ebene, im Team und in der Organisation zu verbessern.

---

**Q**: Was sind Aufgaben des WM?

**A**: Das sind:

* **Identifizieren**
    * Festlegend er Kernkompetenzen und Wissensbereiche
* **Erfassen**
    * Formalisieren des vorhandenen Wissens
* **Speichern**
    * Speichern unter Verwendung entsprechender Schemata
* **Auswählen**
    * Bewertung von Wissen, Relevanz, Wert und Genauigkeit
* **Teilen**
    * Automatisches verteilen von Wissen an Benutzer
* **Anwenden**
    * Abrufen und Nutzen von Wissen
* **Erzeugen**
    * Entdecken von neuem Wissen durch Forschung
* **Verkaufen**
    * Vermarkten von Produkten und Dienstleistungen

---

**Q**: Nennen Sie Drei Säulen des Wissensmanagements

**A**: Das sind:

* **Organisation**
    * Integration von WM Aktivitäten in die Organisationsstruktur und die GP eines Unternehmens
* **Menschen**
    * Bewusstsein steigern und  zur Verwendung WM Technologien motivieren
* **Technologie**

---

**Q**: Welche Basistechnologien von Wissensmanagementsystemen kennen sie?

**A**: Es gibt:

* Klassische Wissensbasen
    * logische Formalismen
* Begriffliche Repräsentationssysteme
    * Semantische Netze
* Prozessorientierte Techniken
    * Scripts, Workflows
* Community-orientirte Techniken
    * Groupware, Social Netzworks
* Agenten
* Service-orientierte Architekturen

---

**Q**: Was ist das Ziel von **Business-Intelligence**?

**A**: Die Analyse unternehmensweit verfügbaren Wissens

---

**Q**: Was versteht man unter dem Begriff "Business-Intelligence"?

**A**: Das ist:

* Kernapplikationen, die eine Entscheidungsfindung unmittelbar unterstützen
    * OLAP
    * MIS bzs. EIS
* Sämtliche Anwendungen, bei denen der Entscheider direkt mit dem System arbeitet
    * Text Mining
    * Ad-hoc Reporting
* Alle direkt und indirekt für die Entscheidungsfindung eingesetzte Anwendungen
    * Auswertungs- und Präsentationsfunktionalität

Von der Datenbereitstellung zur Analyse

* **Data Warehouse**
    * Integriert Informationen aus vielen unterschiedlichen Quellen in einer für die Entscheidungsfindung optimierten Datenbank
    * Fach-orientierte **Strukturierung** der Daten
    * Integration
    * Nicht-Volatilität
    * Historienhaltung
* **Multidimensionale Datenräume**
* Online Analytical Processing (**OLAP**)
    * IS, das es dem benutzer ermöglicht, komplexe Antragen Ad-hoc zu formulierenund Analysen durchzuführen
    * **FASMI**-Kriterien
    * Operationen, z.B. Pivotierung, Roll-Up, Drill-Down, Drill-Accross, etc.
* **Managementinformationssysteme**
    * Berichtsorientierte Analysesysteme
    * Sind auf Planung ausgerichtet
    * Nutzen primär interne operative Daten
* **Data und Knowledge Mining**
    * Nicht-trivialer Prozess des Auffindens von validen und potenziell nützlichen Mustern in Daten
    * Aufgabenklassen:
        * Klassifikation
            * Einordnung einer Beobachting in eine von **n** Klassen
        * Regression
            * Lernen einer Funktion zur Abbildung einer Beobachtung auf einen Zahlenwert
        * Clustering
            * Einteilung von Datensätzen in Gruppen
        * Abhängigkeitsanalyse
            * Erkennen von gesetzmäßigen Anhängigkeiten in Daten
---

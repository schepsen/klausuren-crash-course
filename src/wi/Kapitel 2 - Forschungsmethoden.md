# Kapitel 2: Forschungsmethoden

---

**Q**: Was sind empirische Forschungsmethoden?

**A**: Vorgehensweisen zur Gewinnung und Überprüfung von Erkenntnis

---

**Q**: Was sind empirische Studien?

**A**: Wissenschaftliche Untersuchung der Wirklichkeit

* Systematische Zerlegung eines Problem in eine Anzahl Forschungsfragen
* Wahl der geeigneten Forschungsmethoden für jede Forschungsfrage

---

**Q**: Nennen Sie die wichtigstem Forschungsmethoden

**A**: Das sind:

* Inhaltsanalyse
    * TBA
* Laborexperiment
    * TBA
* Denkmethoden
    * TBA
* **Aktionsforschung** (Nicht kontrollierbare Forschungsumgebung)
    * Forschende sind Untersuchungsteilnehmer
    * Teilnehmer entscheiden mit, welche Ziele verfolgt und welche  Instrumente eingesetzt werden
    * Teilnehmer sind an der Interpretation der Ergebnisse beteiligt
* **Befragung**
    * TBA
* **Fallstudie**
    * Untersuchung eines Phänomens in seiner natürlichen Umgebung ohne Kontrolle oder Manipulation
        * Quantitative bzw. Qualitative Fallstudie
        * Generalisierung über Anzahl der Untersuchungseinheiten
* **Feldexperiment**
    * Hypothesen über Kausalzusammenhänge werden in ihrem natürlichen Umfeld erforscht
        * Manipulation unabhängiger Variablen
* **Simulation**
    * Durchspielen von Szenarien anhand eines Simulationsmodells
        * Deterministische bzw. Stochastische Modelle
* **Synopse** (Review, Literaturstudie) Erhebung des Standes der Forschung auf einem Forschungsgebiet
    * Beschreibung der Literatur in narrativer Form
    * Strukturierung der Inhalte nach geeigneten Kriterien
    * Bewertung der referierten Arbeiten

---

**Q**: Nennen Sie die wichtigstem Forschungstechniken

**A**: Das sind:

* Erhebungs- und Erfassungstechnik
    * Interview
    * Beobachtung
    * Fragebogen
    * Selbstaufschreibung
    * Dokumentationsauswertung
* Beschreibungs- und Darstellungstechnik
    * Freier Text, Grafische Darstellung
* Analyse- und Diagnosetechnik
* Implementierung
* Kreativitätstechnik bzw. Präsentationstechnik

---

**Q**: Was versteht man unter dem Begriff "Design-Science"?

**A**: Darunter versteht man ein Konzept zur Entwickelung von IS Artefakten

Vorstellbare Gliederung:

* Environment
    * People
    * Organization
    * Technology
* IS-Research
    * Develop/Build
    * Justify/Evaluate
* Knowledge Base
    * Fundations
    * Methodologies

---

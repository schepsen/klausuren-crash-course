# Kapitel 3: Cloud Management

---

**Q**: Was sind die Hauptaufgaben des Cloud Managements?

**A**: Das sind:

* Steuerung
    * Re-und Konfiguration der Cloud Umgebung
        * Komplexe Entscheidungsaufgabe
        * Finden einer idealen Konfiguration ist **NP-vollständig**
* Überwachung
    * Auslastung messen
        * CPU Nutzung
        * Speicherverbrauch
        * Festplattenzugriff
        * Netzwerkverkehr
    * Erfüllungsgrad der SLA's (Dienstgütevereinbarungen) beobachten
        * Verfügbarkeit
        * Durchsatz
        * Reaktionszeiten
    * Prognosen erstellen
        * Meist schwierig
        * Angebote folgen oftmals dem Best-Effort-Prinzip

---

**Q**: Was für Engpässe bzw. Seiteneffekte können bei der VM-Zuordnung auftreten?

**A**: Das sind:

* Service Awareness
    * Unterschiedlicher Ressourcenverbrauch
    * Einseitige Anforderungen führen zu Engpässen auf PM's
* Robustheit der Cloud Umgebung
    * Kololation von VM's, deren Ausfall kritisch für einen Dienst wäre, schächt die Robustheit
* Stabile Konfiguration
    * Rekonfiguration verursacht selbst auch Kosten
* Wahrnehmung von Engpässen durch die Benutzer
    * Batch-Prozesse sind weniger kritisch als Benutzerinteraktionen

P.S. Niedrige Energiekosten versprechen höhere Gewinne (!!Allerdings dürfen keine Einbüßen über die SLA's hinausgehen)

---

**Q**: Welche Rekonfigurationsaktionen kennen Sie?

**A**: Das sind:

* Bandbreitenerhöhung
* Erhöhung des Arbeits-bzw. Festspeichers
* Allokation zusätzlicher CPU's
* Migration zu einer separaten VM
* Migration zu einer separaten PM
---

**Q**: Nennen Sie alle möglichen **Eskalationstuffen**

**A**: Das sind:

1. VM-Konfiguration anpassen
2. Anwendung von einer VM zu einer anderen migrieren
3. VM von einer PM zu einer anderen migrieren oder neue VM auf geeigneter PM erzeugen
4. PM an- und abschalten
5. Anwendung zu anderem Cloud-Provider migrieren

---

**Q**: Erläutern Sie den Begriff **MAPE**

**A**: Der Begriff MAPE steht für **M**onitor, **A**nalyze, **P**lan und **E**xecute

---

**Q**: Nennen Sie die möglichen Gefährdungsfaktoren für IT-Sicherheit

**A**: Das sind:

* Höhere Gewalt
    * Blitzschlag
    * Feuer
    * Erdbeben
    * Streik
* Fahrlässigkeit
    * Irrtum
    * Fehlbedingung
* Technisches Versagen
    * Stromausfall
    * Hardware-Ausfall
* Vorsatz
    * Einbruch
    * Hacking
    * Sabotage
* Organisatorische Mängel
    * Unberechtigter Zugriff
    * Raubkopie

---

**Q**: Nennen Sie die möglichen Schutzziele der IT-Sicherheit

**A**: Das sind:

* Vertraulichkeit
    * Schutz vor unbefügtem Informationsgewinn
* Integrität
    * Unversehrtheit von Informationen und Ressourcen
* Verfügbarkeit
    * Unbefügte Beeinträchtigung der Funktion

---

**Q**: Nennen Sie weitere Schutzziele der IT-Sicherheit

**A**: Das sind:

* Informationelle Selbstbestimmung
    * Recht auf Löschung eines Facebook-Accounts
* Verbindlichkeit
* Klare, angemessene Eigentums- und Verwertungsrechte
* Ortssicherheit für Datentransfer und Speicherung

---

**Q**: Nennen Sie die möglichen Schutzmaßnahmen

**A**: Das sind:

* Technische Maßnahmen
    * Authentifizierung
    * Kryptografische Verfahren, z.B. Zertifikate
    * Anonymisierung
* Rechtliche Maßnahmen
    * Vertraglich zugesicherte Rechte und Pflichten (SLA)
* Organisatorische Maßnahmen
    * Backup- und Recovery-Strategien
* Bauliche Maßnahmen
    * Versiegelte Server
    * Zugangskontrolle

P.S. Offene rechtliche Fragen: Rechtsstandorte, Forensik, Sorgfaltspflicht

---

**Q**: Was ist ein Risiko?

**A**: Risiko = Eintrittswahrscheinlichkeit * Schadenshöhe

P.S. Vollständige Sicherheit gibt es nicht!

---

**Q**: Was sind die Hauptziele der Standardisierungsbemühungen?

**A**: Das sind:

* Innovationen durch Industriestandards
* Vermeidung der Abhängigkeit von einzelnen Provider
* Offene Systeme

Zentrale Themen sind:

* Interoperabilität und Integration
    * Standardisierung von Schnittstellen
    * Entwicklung von Mediatoren
* Sicherheit
* Portabilität
* GRC (Governance - Risikomanagement - Compliance)

---

**Q**: Welche cloud-spezifische Standards kennen Sie?

**A**: Das sind:

* Packaging
    * OVF
        * Konfigurationsmöglichkeit der Parameter für Daten-und Rechenspeicher und Netzwerke
    * Tosca
* Cloud Managements
    * OCCI
    * CIMI: Interoperabilität von IaaS-Diensten durch ein REST-basiertes Protokoll
        * Representational State Protokol für die zustandslose Kommunikation
        * GET-, PUT- und DELETE Befehle
        * Ansprechen von IaaS-Ressourcen über URI's
* Cloud Speichermanagement
    * CDMI
        * REST-basierte Schnittstelle zum Transfer von JSON-Objekten

---

**Q**: Erklären Sie den Begriff "Cloudonomics"

**A**: Cloudonomics betrachtet ökonomische Kräfte, Geschäftsfaktoren und strukturelle Einflussgrößen auf Kosten und Nutzen des Einsatzes von Cloud-Technologie

---

**Q**: Nennen Sie die dabei beteiligten Gruppen (Stakeholder)?

**A**: Das sind:

* Nutzer
    * Individuen oder Firmen
* Provider
    * Firma, die ein Cloud-Dienst produziert und betreibt
* Vendor
    * Firma, die unter ihrer Steuernummer einen Cloud-Dienst anbietet

---

**Q**: Nennen Sie die potenzielle Vorteile durch den Einsatz von Cloud-Technologie

**A**: Das sind:

* (N) Einsparpotenzial für IT-Kosten
    * Anschaffung- und Betriebskosten
* (N) Wettbewerbsvorteile
    * HPC, Verfügbarkeit und Skalierung
* (N) Skaleneffekte durch viele Nutzer
    * Wartung
    * Sicherheit
* (V) Neue Geschäftschancen
* (V) Einsparpotenzial durch Standortwahl
* (V) Höhere Rendite durch Resource Pooling

---

**Q**: Nennen Sie die potenzielle Nachteile durch den Einsatz von Cloud-Technologie

**A**: Das sind:

* Integrationsprobleme mit Altsystemen
* Verfügbarkeit der Dienste und Daten
* Lock-In der Daten, sprich Providerabhängigkeit
* Vertraulichkeit
* Performance und  Skalierbarkeit

---

**Q**: Was versteht man unter dem Begriff "**T**otal **C**ost of **O**wnership"

**A**: TCO berücksichtigt alle Kosten, die in Zusammenhang mit der Anschaffung und dem Betrieb einer IT-Komponente stehen

---

**Q**: Erläutern Sie die drei Annahmen für die TCO nach Martens

**A**: Das sind:

1. Internetzugang und Client PC's sind bereits vorhanden
2. Eine Server-Infrastruktur ist nicht erforderlich
3. Wechsel des Cloud-Providers erfolgt vor der Ersteinführung des Dienstes

---

**Q**: Welche Kostenarten gibt es im TCO-Modell?

**A**: Es gibt folgende Kostenarten:

* Strategische Entscheidungen
    * Analyse der IT-Landschaft
    * Wahl des Service-Modells (IaaS, PaaS, SaaS)
    * Wahl des Betriebsmodell (Public, Private, etc.)
* Bewertung und Auswahl von Cloud-Anbietern
    * Recherche von Anbietern
    * Bewertung deren Reputation
    * Bewertung des jeweiligen Angebots
* Preismodelle
* Implementierung, Konfiguration, Integration und Migration
* Support
* Schulung
* Instandhaltung
    * Monitoring
    * SLA-Management
    * Updates
* Systemausfälle
    * Vertragsstrafen
    * Reputationsverlust
* Stilllegung
    * Portierung der Daten aus der Cloud
    * Wiedereingliederung ausgelagerter Prozesse ins Unternehmen
    
---

**Q**: Welche Abrechnungsmodelle kennen Sie?

**A**: Das sind:

* Pay as you Go
    * Zeit-verteilte, tatsächliche Nutzung von Ressourcen
* Leasingmodell
* Bill & Keep
* Cost-based Access Procing
    * Dienstnutzung wird gemessen

---

**Q**: Nennen Sie die Anforderungen an SLA's

**A**: Das sind:

* Verständlichkeit
* Verhandelbarkeit
* Einhaltbarkeit bzw. Erfüllbarkeit

---

**Q**: Nennen Sie die wichtigsten Strategien für die Kapazitätsplanung

**A**: Das sind:

* JTL
    * Eingreifen erfolgt erst nach Verletzung der SLA
    * Leichte Verluste durch Vertragsstrafen
* JIC
    * Leicht überhöhte Kapazitäten, was zu höheren Ausgaben führen
* JIT
    * Größter ökonomischer Nutzen
    * Erfordern genaue Analyse-und Monitoring Werkzeuge

P.S. Die Wahl der Strategie hängt davon ab, wie wichtig Skalierbarkeit ist

---

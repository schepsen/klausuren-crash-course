# Kapitel 2: Architekturen des Cloud Computing

---

**Q**: Nennen Sie die wichtigsten Schichten der CC-Architektur

**A**: Das sind:

* SaaS (**S**oftware **a**s **a** **S**ervice) richtet sich an Endkunden
    * Anwendungen
        * Funtionalität mit komplexer Natur, z.B. Microsoft Office
    * Anwendungsdienste
        * Funtionalität basiert auf einer einzigen Anwendung, z.B. Google Maps

* **Beachte**: Lokale SW-Installation auf Kundenseite entfällt

Provider | Schicht | Kundenseite
:-------:|:-------:|:----------:
Admin Control | Apps | Limited Admin Control
Total Control | Middleware<br>OS<br>Hardware | No Control

* PaaS (**P**latform **a**s **a** **S**ervice) richtet sich meist an Entwickler

    * Entwicklungsumgebungen, z.B. Microsoft Azure, Force.com von Salesforce
        * Eigene SW lässt sich in einer bestimmen PS entwickeln        
        * Programmiersprachen        
        * Compiler        
        * Testwerkzeuge        
    * Laufzeitumgebungen, z.B. RedHut OpenShift
        * Eigene SW lässt sich darin ausführen        
        * Virtuelle Maschinen
        * Laufendes Server-Programm, das Anfragen von Clients bedienen kann                
    * ** Ungefährer Ablauf**:     
        * Provider stellt virtuelle Maschinen und OS zur Verfügung
        * Provider bietet in der Middleware Zugriff auf Entwicklungswerkzeuge und Interfaces an
        * Monitoring seitens Kunden
        * Kunde kann Anwendungen als SaaS an Dritte anbieten

Provider | Schicht | Kundenseite
:-------:|:-------:|:----------:
No Control | Apps, z.B. Mail | Admin Control
Admin Control | Middleware, z.B. Java | API bzw. Interfaces
Total Control | OS<br>Hardware | No Control

* IaaS (**I**nfrastructure **a**s **a** **S**ervice) richtet sich an Systemadministratoren
    * Infrastrukturdienste
        * Rechendienste
            * Hadoop  **Map-Reduce**, für z.B. Data Mining und Maschinelles Lernen
        * Speicherdienste
            * Amazon S3
            * DropBox
            * Google Cloud Storage
    * Ressourcenverwaltung
        * Amazon (Elastic Compute Cloud) EC2
        * Eucalyptus
    * **Ungefährer Ablauf**:
        * Provider stellt virtuelle Ressourcen zur Verfügung
        * Kunde installiert eigene Anwendungen, Middleware, OS
        * Skalierung von Kapazitäten auf Anfrage des Kunden

Provider | Schicht | Kundenseite
:-------:|:-------:|:----------:
No Control | Apps<br>Middleware<br>OS | Total Control
Admin Control | Hypervisor | Allow to make Requests
Total Control | Hardware | No Control


P.S. Die höhere Schichten können die Dienste der tieferen Schichten nutzen

---

**Q**: Gibt es auch weitere Schichten?

**A**: Ja, das sind:

* HaaS (**H**umans **a**s **a** **S**ervice)
    * Crowd Sourcing, z.B. Amazon Mechanical Turk
        * Menschliche Ressourcen für Aufgaben
* BPaaS (**B**usiness **P**rocess **a**s **a** **S**ervice)
    * Outsorcing von Geschäftsprozessen auf Grundlage von Cloud Technologie, z.B. DATEV
* DaaS (**D**esktop **a**s **a** **S**ervice)
    * Virtuelle Desktops statt herkömmlichen Betriebssysteme
* DbaaS (**D**ata**b**ase **a**s **a** **S**ervice)
    * Operationen und Funktionen eines entfernt liegenden Datenbankmanagementsystems

---

**Q**: Erklären Sie die IaaS-Schicht

**A**: Sie bietet eine abstrahierte Sicht auf Hardware an

---

**Q**: Beschreiben Sie das Map-Reduce-Modell

**A**: Map-Reduce wird zur massiven parallelen Verarbeitung von Daten eingesetzt, und besteht aus zwei Schritten:

* **Map**: liest Schüssel/Wert-Paare ein und generiert daraus neue Schlüssel/Wert-Paare als Zwischenergebnis
* **Reduce**: gruppiert die Ausgabe nach Schüsseln und generiert daraus eine aggregierte Ausgabe für jeden Schlüssel

* beruht auf dem **Master-Slave**-Prinzip
    * Einem Masterknoten wird eine Aufgabe zugewiesen
    * Masterknoten zerlegt die Eingabedaten und erzeugt Unteraufgaben
    * Jedem verfügbaren Slaveknoten wird eine Map- bzw. Reduceaufgabe zugewiesen
    * Ausführung erfolgt parallel
    * Zwischenergebnisse werden regelmäßig sortiert

---

**Q**: Erklären Sie die Amazon Elastic Compute Cloud (EC2)

**A**: Wichtige Merkmale:

* Virtuelle Server als EC2 Instanzen, die aus AMI erstellt werden
    * Instanzen ID
    * Status der Instanz
    * Instanztyp (CPU | Plattenplatz | Arbeitsspeichergröße)
    * Verfügbarkeitszone
* Amazon Machine Images (AMI) (OS + vorinstallierte Anwendungen)
* Drittanbieter verkaufen vorkonfigurierte AMI's
* Persistenz und Sicherung
    * Speicherung von Zuständen
    * Momentaufnahme (Backups)
    * Sicherheitsgruppe (virtuelle Firewalls)

---

**Q**: Was ist Eukalyptus?

**A**: Das ist ein IaaS-Angebot und besteht aus:

* Cloud-Controller (**CLC**) es gibt genau **EINEN** CLC
    * Verwaltungsinstanz der Cloud, z.B. stellt API, manages Ressourcen, bearbeitet Requests
* Walrus
    * Bucket-basierte Speicherkomponente
    * Ebenfalls einzigartig wie CLC
    * Speichert die virtuellen Maschinen der Cloud
    * Kann als Storage as Service genutzt werden
* Cluster-Controller (**CL**)
    * Frontend für einen Cluster
    * Manages die Ausführung von virtuellen Maschinen und den SLA's pro Cluster
* Storage-Controller (**SC**)
    * Speicherkomponente des Clusters
    * Kommuniziert mit CC und NC zur Verwaltung des Speichers innerhalb des Clusters
* Node-Controller (**NC**)
    * Hosts Virtuelle Maschinen
    * Betreibt EBS-(SC)-Instanzen

---

**Q**: Erklären Sie die Amazon Simple Storage Service (S3)

**A**: Wichtige Merkmale:

* Bucket: flacher Namensraum, Zugriff über Web Services
* Objekte bis zu einer Größe von 5 GB
* Authentifizierung über Schlüssel
* Langsamer Speicherzugriff auf Daten

---

**Q**: Erklären Sie die PaaS-Schicht

**A**: Grundlage für Entwickler, um skalierbare Anwendungen zu erstellen

**Szenarien**:

* Provider lässt Anwendungen für einen Kunden Laufen
* Entwickler nutzt die Entwicklungswerkzeuge des Providers
* Entwickler erstellt mit Hilfe der Entwicklungswerkzeuge Anwendungen, die in der Infrastruktur des Providers laufen
* Administrator konfiguriert eine Anwendung in der Infrastruktur des Providers

---

**Q**: Erklären Sie das Prinzip von **RedHut OpenShift**

**A**: Das ist:

* Containerbasierte Laufzeit- und Entwicklungsumgebung
* Benutzt die Open-Source-Technologie
* Unterstützt folgende Sprachen: Java, Ruby, Node.js, Python, u.a.
* Unterstützt relationale und NoSQL Datenbanken
---

**Q**: Erklären Sie **Microsoft Windows Azure**

**A**: Das ist eine Plattform zur Entwicklung, Bereitstellung und Verwaltung von Anwendungen auf Datencentern

Windows Azure besteht aus folgenden Komponenten:

* Connect
    * Aufbau von IP-basierten Verbindungen zwischen den lokalen Rechnern und Azure
* Compute
    * Ausführen von Anwendungen in der Cloud
* Storage
    * **Table** (sehr einfache Tabelle ohne relationales Schema)
    * **Blob** (binäres Datenobjekt bis zu 1 TB mit Metadaten)
    * **Queue** (Nachrichtenaustausch)
    * Speichern binärer und strukturierter Daten sowie Kommunikationsnachrichten
* CDN
    * Beschleunigung des Datenzugriffs    
* Fabric Controller
    * Verwaltet alle Ressourcen einer Windows Azure Anwendung

---

**Q**: Welche Rollen von **Microsoft Windows Azure** kennen Sie?

**A**: Das sind:

* **Web**-Rolle
    * Zur Erstellung und Ausführung von Webbasierten Anwendungen
* **Worker**-Rolle
    * Zur Ausführung von Code
* **VM**-Rolle
    * Zur Ausführung von Server-Images

P.S. Load Balancer nimmt Anfragen entgegen und verteilt sie auf die Instanzen einer Rolle

---

**Q**: Beschreiben Sie die **Force.com** Plattform

**A**: Das ist:

* Cloud-Entwicklungsplattform mit mehr als 100.000 Kunden
* Metadaten-getriebene Architekturen
* Eigens entwickeltes relationales Datenbanksystem als Kern
* Multimandatenfähiges Datenmodell
* Persistenz der Daten wird dank des Transaktionsmanagements gesichert
* Verwendet eine prozedurale Programmiersprache namens **Apex**

---

**Q**: Was versteht man unter dem Begriff "Inkrementelle Migration"

**A**: Das Zielsystem wird in kleinen Schritten, beispielsweise Komponente für Komponente, aufgebaut und parallel zum Altsystem betrieben

---

**Q**: Welche Anforderungen seitens des Providers existieren in SaaS?

**A**: Das sind:

* Bereitstellung, Konfigurieren, Aktualisieren und Verwalten der Anwendungen
* Durchsetzen von SLA
* Abrechnung
* Problembehebung

---

**Q**: Erklären Sie die Rolle der Middleware

**A**: Das sind:

* Bietet SW-Bausteine für Anwendungen, z.B. Bibliotheken, Interpreter, Aufrufe von Netzwerkdiensten
* Häufige Komponenten
    * Datenbank-Dienste
    * Accountmanagement
    * Benutzerauthentifizierung

---

**Q**: Nennen Sie alle Isolationsstrategien von Kundendaten, die Sie kennen

**A**: Jeder Kunde bekommt eine eigene Instanz der Anwendung

1. Auf demselben physischen Rechner
2. Auf einer separaten VM
3. Auf einem separaten physischen Rechner
4. Mehrmandatfähige Anwendung mit gemeinsamer Datenbank

---

**Q**: Beschreiben Sie die **Salesforce** Anwendung

**A**: Software zur Verwaltung von Kundenbeziehungen (CRM)

---

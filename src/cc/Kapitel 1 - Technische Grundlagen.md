# Kapitel 1: Technische Grundlagen

---

**Q**: Was ist ein Cloud-Dienst?

**A**: Ein im Web verfügbarer Dienst, der keine Installation erfordert, sondern **an-hoc** genutzt werden kann

---

**Q**: Was ist Cloud-Computing?

**A**: CC erlaubt die Bereitstellung und Nutzung von IT-Infrastruktur, von Plattformen und von Anwendungen aller Art als Cloud-Dienste

---

**Q**: Was sind die Merkmale des Cloud-Computing?

**A**: Das sind:

1. On-demand self-Service
    * Diensterbringung auf Anfrage, und zwar ohne jegliche menschliche Interaktion
2. Netzwerkbasierter Zugang
    * z.B.  Internet
3. Resource Pooling
    * Ressourcen sind in Pools verfügbar
    * Anpassung erfolgt an tatsächlichen Bedarf
4. Elastizität
    * Illusion unendlich verfügbarer Ressourcen
    * Einfache Skalierbarkeit
5. Messbarkeit: quantative bzw. qualitative Messbarkeit der Dienste

---

**Q**: Was ist eine Virtualisierung?

**A**: Abstrakte, logische Sicht auf physische Ressourcen. P.S. Sie ist die wichtige Voraussetzung für die **ELASTIZITÄT**

---

**Q**: Welche Virtualisierungskonzepte kennen Sie bereits?

**A**: Das sind:

1. Betriebssystemvirtualisierung
    * Betriebssystemkern, auf dem mehrere virtuelle Systemumgebungen laufen      
2. Plattformvirtualisierung
    * Ausführung beliebiger Betriebssysteme und Anwendungen in virtuellen Umgebungen
    * **Hypervisor** ist ein auf ein Minimum reduziertes Metabetriebssystem, das die HW-Ressourcen unter den Gastsystemen verteilt
3. Speichervirtualisierung
    * Datenspeicher ist getrennt von den klassischen File-Servern, z.B. Speichernetzwerk (SAN)
4. Netzwerkvirtualisierung
5. Anwendungsvirtualisierung
    * Dynamische Bereitstellung von SW-Komponenten zum Download oder zum Betrieb über das Internet (hosted application)

---

**Q**: Was ist SOA?

**A**: Service-Orientierte Architekturen bestehen aus voneinander unabhängigen Diensten

---

**Q**: Nennen Sie das Paradigma der SOA

**A**: Das sind:

* Lose gekoppelte SW-Komponenten
* Komponenten bieten Dienste an
* Komponenten kommunizieren über ein Netzwerk miteinander

---

**Q**: Nennen Sie die Rollen der SOA

**A**: Das sind:

* Service-Requestor
    * möchte einen Dienst nutzen
* Service-Provider
    * stellt Dienste zur Verfügung
* Service-Broker
    * findet passende Provider für einen Requestor

---

**Q**: Was ist ein Web-Service?

**A**: Dienst, der seine Schnittstelle über Standard-Web-Protokolle anbietet

* Der Service wird  durch **WSDL** beschrieben
* Der Dienst selbst kann in beliebiger **PS** implementiert werden
* Kommunikation mit dem Web-Service erfolgt über Webprotokolle
    * HTTP
    * SMTP
    * SOAP (Simple Object Access Protokoll)
* **UDDI** ist ein Dienstverzeichnis

---

**Q**: Welche Betriebsmodelle des CC kennen Sie?

**A**: Das sind:

1. Public Cloud
    * Anbieter und potentielle Nutzer gehören nicht derselben organisatorischen Einheit an
2. Private Cloud
    * Anbieter und Nutzer gehören derselben organisatorischen Einheit an
3. Hybrid Cloud
    * Eine Kombination aus Private & Public Clouds, wobei einige Dienste in die Public Clouds ausgelagert werden
4. Community Cloud
    * Die Cloud Infrastruktur gehört mehreren Organisationen an

---

**Q**: Nennen Sie die 4 Dimensionen des Cloud Cube

**A**: Das sind:

* Physischer Speicherort der Daten (Intern bzw. Extern)
* Besitzverhältnisse der Technologie (Open-Source bzw. Proprietär)
* Sicherheitsbereiche der Architektur (Innerhalb bzw. Außerhalb der Firewall)
* Betreiber der Cloud (Eigenbetrieb bzw. Fremdbetrieb)

---

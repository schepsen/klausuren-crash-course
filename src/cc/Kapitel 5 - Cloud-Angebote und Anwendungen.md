# Kapitel 5: Cloud-Angebote und Anwendungen

---

**Q**: Was sind aktuelle Trends?

**A**: Das sind:

* Hybride Cloud-Lösungen
    * VMWare vCloud Hybrid Service
        * Erweiterung des Rechenzentrums in die Cloud
    * HP Public Cloud
        * Erleichtert die Portabilität zwischen HP-eigenen und Kunden-eigenen Rechenzentren
* Consumer Cloud
    * Dienste für Privatkunden
        * Funktionalität ist immer und überall
        * Große Menge an Daten
        * Dienste beobachten den Kontext, in dem ein Nutzer sich gerade befindet
* Scientific Data Cloud
    * Cloud-Dienste, die auf e-Science spezialisiert sind

---

**Q**: Was sind Einflussfaktoren auf das Kundenverhalten?

**A**: Das sind:

* Kosten
* Nahtloser Übergang von Private- zu Arbeitsumfeld
* Markentreue
* Kundenerlebnisse
* Integrationsfähigkeit der Dienste

---

**Q**: Welche Unterschiede gibt es zwischen Consumer und Enterprise Cloud?

**A**: Das sind:

* Strenge Regeln für Governance, Sicherheit, Datenschutz und SLA's für Privatkunden oft irrelevant
* Anbieter können Geschäftsfälle für Firmenkunden klarer beschreiben und Erfolgsprognosen abgeben
* Strafzahlungen bei Vertragsbrüchen wirken sich bei Firmenkunden stärker aus

Consumer Dienste:

* Medien
    * Fernsehen
    * E-Books
    * Music aus der Cloud
* Bildung
    * Virtuelle Klassenzimmer
    * Lernspiele
    * Cloud Academy
* Auto
    * Kundennetzwerke
* Intelligentes Haus
    * Licht- und Heizungsteuerung

---

**Q**: Was sind Merkmale von Grid Computing?

**A**: Das sind:

* Dezentrale Administration von Verarbeitungsressourcen
* Verwendung offener Standards
* Umfassende Unterstützung von Dienstgütemerkmalen

Konzept zur Aggregation und gemeinsamen Nutzung von heterogenen, vernetzen Ressourcen in einem verteilten System

---

**Q**: Welche Grid-Schichten kennen Sie?

**A**: Es gibt:

* Fabrik-Schicht
    * Integration von Ressourcen in die Grid-Infrastruktur
* Kommunikationsschicht
    * Kommunikation und Authentifizierung
* Ressourcen
    * Zugriff auf einzelne Grid-Ressourcen
* Verbund
    * Ressourcenübergreifende Dienste zur Verwaltung von Ressourcen
* Anwendungsschicht
    * Anwendungen, die auf Grid-Ressourcen zugreifen

---

**Q**: Welche Grid-Modelle gibt es?

**A**: Es gibt:

* **Rechen**-Grids
* **Daten**-Grids
* **Service**-Grids

---

**Q**: Gibt es Unterschiede zwischen Grid und Cloud Computing?

**A**: Ja, das sind:

Grid-Computing | Cloud-Computing
:-------------:|:----------------:
Ressourcen-orientierte Sichweise | Benutzer- und Aufgaben orientierte Sichtweise
Pool an heterogenen Ressourcen | Pool an homogenen Ressourcen
Zugriff über einheitliche Schnittstelle | Individuelle Ressourcenvergabe
------|------
Projekt-orientiert | Gewinn-orientiert
Benutzer bieten Ressourcen an | Cloud-Industrie bietet Ressourcen an
Benutzer dürfen diese Ressourcen nutzen | nur gegen Bezahlung

---

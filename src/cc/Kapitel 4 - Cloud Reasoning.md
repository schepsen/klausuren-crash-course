# Kapitel 4: Cloud Reasoning

---

**Q**: Nennen Sie die zwei Richtungen des Cloud Reasonings

**A**: Das sind:

* Einsatz vom Cloud-Technologie für das Reasoning
    * Maschinelles Lernen
* Einsatz von Reasoning-Methoden für Cloud-Aufgaben
    * Cloud-Management

---

**Q**: Beschreiben Sie den k-means-Algorithmus

**A**:  Der Algorithmus besteht aus 4-Schritten:

* Wähle zufällig **k** Clusterzentren
* Ordne Objekte dem Cluster mit dem nächsten Clasterzentrum zu
* Berechne die Zentren der Cluster neu
* Verteile und ordne Objekte erneut dem Cluster mit dem nächsten Clasterzentrum zu

---

**Q**: Was ist ein Software-Agent?

**A**: Ein SW-Agent ist ein längerfristig arbeitendes Programm, dessen Arbeit als eigenständiges Erledigen von Aufträgen in Interaktion mit einer Umwelt beschrieben werden kann

---

**Q**: Erläutern Sie den **S**ense-**T**hink-**A**ct-Zyklus

**A**: Die wichtigsten Punkte:

1. Informationsaufnahme
2. Wissensverarbeitung und Entscheidung
3. Aktionsausführung

---

**Q**: Erläutern Sie das exploratives Projekt "CloudBay"

**A**: Das ist eine Plattform für den Handel von Rechenressourcen in einer offenen Cloud-Umgebung

* **Buy-it-Now** - Sofortkauf für einen Festpreis
* **Bid** - Auktion mit Geboten verschiedener Kaufinteressenten

---

**Q**: Beschreiben Sie die Ausubel-Auktion

**A**: Grundidee:

1. Bieter reichen Preisangebote ein
2. Auktionator nennt einen Preis
3. Jeder Bieter gibt an, wie viel er zu diesem Preis kaufen würde
4. Teilabschlüsse werden berechnet
5. Falls Angebot >= Nachfrage gilt, so wird die Auktion beendet, sonst erhöht man den Preis und geht zum 3. Punkt

P.S. Die dafür geltenden **Voraussetzungen** sind:

* Bieter bieten monoton
* Einheiten dürfen nicht zurückgegeben werden

Ausubel in zwei Schritten:

* Bestimmung des Preises, der den Markt klärt
    1. Alle Gebote nach den aufsteigenden Preisen sortieren
    2. Liste halbieren und gebot in der Mitte als potenziellen Marktpreis wählen
    3. Wiederhole die obere Punkte
    4. Falls keine optimale Lösung gefunden wird, wähle den Preis so, dass möglichst wenig Ressourcen übrig bleiben
* Kalkulation der Preise durch Anwenden des Clinching-Verfahrens

---

**Q**: Erläutern Sie die Regelbasierten Systeme für Cloud-Management

**A**: Das Ziel ist die intelligente Steuerung der Aktionen zur Rekonfiguration von Cloud-Umgebungen

* Wissensanwendung findet in Regelform "WENN-DANN" statt
* Faktenmenge repräsentiert den aktuellen Wissensstand und dementsprechend erweiterbar
* Ein Interpreter steuert den Ablauf
    * Matching
    * Konfliklösung
    * Aktion

---

**Q**: Was sind Workflows?

**A**: Workflows sind die Automatisierung eines Geschäftsprozesses im Ganzen oder in Teilen, wodurch Dokumente, Informationen oder Aufgaben in einer durch Regeln festgelegten Reihenfolge von einem Bearbeiter zu einem Nächsten gereicht werden

---

**Q**: Erklären Sie den Begriff "Task Placement"

**A**: Zuweisung jedes Workflows zu einer VM oder einen PaaS-Container bzw. jeder Vm zu einer PM

P.S. **T**ask **P**lacement **C**ase ist ein Paar aufeinander folgenden TP mit den dazugehörigen Rekonfigurationsaktionen

---

**Q**: Nennen Sie Vorteile einer tiefen Integration Workflow-Cloud

**A**: Das sind:

* Für den Cloud-Service-Provider:
    * Neue Geschäfftsfelder
    * Green Cloud
    * Weiterentwicklung von Cloud-Standards
* Für den Workflow-Nutzer:
    * Elastizität von Geschäftsprozessen
    * Höhere Performance bei der Workflow-Ausführung
    * Einsatz von Cloud-Tools

---

**Q**: Nennen Sie existierende Policy Modes

**A**: Das sind:

* Grün
    * Es gibt viele ungenutzte Ressourcen
    * Mehrverbrauch ist erlaubt
* Grün-Orange
    * Mehrverbrauch wird reduziert
* Orange
    * Ressourcen werden knapp
    * Mehrverbrauch ist ausgeschlossen
* Orange-Rot
    * Mehrverbrauch ist ausgeschlossen
    * Outsourcing wird eingeleitet
* Rot
    * Mehrverbrauch ist ausgeschlossen
    * Outsourcing wird auf Kosten von Kunden mit niedriger Reputation eingeleitet

---

**Q**: Was ist der Lösungsansatz für die tiefe Integration von Workflow-Management?

**A**: Das sind:

* Schichtenübergreifendes Monitoring
* Fallbasierte (Re-)Konfiguration

---

**Q**: Was tut eine WfMS?

**A**: Ein WfMS definiert, erzeugt und verwaltet die Abarbeitung von Workflows mit Hilfe von Software

---

**Q**: Was ist die Herausforderung eines WFaaS Vendors?

**A**: Das ist Maximierung des Gewinns durch:

* Optimierung des Preismodells
    * Vermeidung von Überkapazitäten
    * Akkurate Vorhersage der Kosten
    * Flexibilität bei der Wahl des Providers und WfMS
* Vermeidung von SLA Verletzungen
    * Frühzeituges Erkennen von Unterkapazitäten
    * Schnelles Reagieren auf Ausnahmesituationen

---

**Q**: Welche mögliche Tiefen der Integration kennen Sie?

**A**: Das sind:

* Keine Integration
    * WfMS und Tasks werden getrennt voneinander in der Cloud ausgeführt
    * Keine direkte Ableitung des Bedarfes für Ressourcen-Skalierung aus den Workflows
    * Schnelle Bildung der Über-bzw. Unterkapazität
* Tiefe Integration
    * WfMS steuert direkt die Cloud
    * Gewährleistet die optimale Ressourcennutzung
    * Kann zu einem Vendor- Lock-In führen
* Intelligente Integration
    * Eine Monitorung-und Management-Software überwacht WfMS und die Workflows

---

**Q**: Was ist ein WFCF?

**A**:  Das ist ein Konnektor-basiertes Framework, dessen Ziel ist,  Über-bzw. Unterkapazitäten zu reduzieren und gleichzeitig dem Vendor eine flexible Wahl des WfMS und des Providers zu ermöglichen

---

**Q**: Was beschreibt eine Workflowdefinition?

**A**: Sie beschreibt:

* Struktur eines Workflows
* Alle möglichen Pfade entlang des Workflows
* Regeln für die Wahl der Pfade
* Alle Tasks

---

**Q**: Was ist eine Workflowinstanz?

**A**: Das ist ein ausführbarer Workflow, der von einer WFD abgeleitet wurde

---

**Q**: Erklären die den **MAPE**-zyklus bei dem WFCF

**A**: Der MAPE-Zyklus:

1. Monitor (wird von **CWorkload** erfasst)
    * Überwachung der Workflowdefinitionen
    * Überwachung der Workflowinstanzen
    * Überwachung der Cloud Ressourcen
2. Analyse (**CProblem** & **CSimu**)
    * Ob es jetzt oder in näherer Zukunft Probleme gibt
3. Plan
    * WFCF Solver sucht in seiner Case-Base nach ähnlichem Fall und benutze die Lösung als Ansatz
4. Execute
    * Fertige Lösung wird nun umgesetzt

Um Ressourcenbedarf besser planen zu können, ist es sinnvoll, die ausführenden Tasks zu charakterisieren
* Long Running
* Data Depending
* CPU Intensive
* Memory Intensive

**Post-Mortem** Analyse untersucht einen Basis-Fall nachträglich, um eine optimale Lösung zu finden

---

## PROJECT ##

* ID: **K**lausuren!**M**ARKDOWN
* Contact: schepsen@web.de

## USAGE ##

TBA

## CHANGELOG ##

### Klausuren!MARKDOWN 0.2, updated @ 2019-03-22 ###

* added WI - Kapitel 5: Forschungstrends der Wirtschaftsinformatik
* added WI - Kapitel 4: SOA und Webtechnologien

### Klausuren!MARKDOWN 0.2, updated @ 2019-03-21 ###

* added WI - Kapitel 3: Anwendungssysteme

### Klausuren!MARKDOWN 0.2, updated @ 2019-03-20 ###

* added WI - Kapitel 2: Forschungsmethoden
* added WI - Kapitel 1: Grundbegriffe und Selbstverständnis

### Klausuren!MARKDOWN 0.1, updated @ 2019-03-19 ###

* added CC - Kapitel 5: Cloud-Angebote und Anwendungen
* added CC - Kapitel 4: Cloud Reasoning
* added CC - Kapitel 3: Cloud Management

### Klausuren!MARKDOWN 0.1, updated @ 2017-03-16 ###

* added CC - Kapitel 2: Architekturen des Cloud Computing

### Klausuren!MARKDOWN 0.1, updated @ 2017-03-15 ###

* added CC - Kapitel 1: Technische Grundlagen
